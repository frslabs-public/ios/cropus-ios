

Pod::Spec.new do |s|
  s.name             = 'Cropus'
  s.version          = '1.0.0'
  s.summary          = 'Cropus is used to capture and crop signature'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/cropus-ios'
  s.license          = 'MIT'
  s.author           = {'sravani' => 'shravani@frslabs.com' }

s.source           = {:http => 'https://cropus-ios.repo.frslabs.space/cropus-ios/1.0.0/Cropus.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '11.0'
  s.ios.vendored_frameworks = 'Cropus.framework'
  s.swift_version = '5.0'
end
