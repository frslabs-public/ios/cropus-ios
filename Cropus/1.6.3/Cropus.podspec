

Pod::Spec.new do |s|
  s.name             = 'Cropus'
  s.version          = '1.6.3'
  s.summary          = 'Cropus is used to capture and crop signature'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/cropus-ios'
  s.license          = 'MIT'
  s.author           = {'sravani' => 'shravani@frslabs.com' }

s.source           = {:http => 'https://www.repo2.frslabs.space/repository/cropus-ios/cropus-ios/1.6.3/Cropus.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '12.0'
  s.ios.vendored_frameworks = 'Cropus.framework'
  s.swift_version = '5.0'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
end
